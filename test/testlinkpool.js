var LinkPool = artifacts.require("LinkPool");

contract('LinkPool', function(accounts) {

    var stakers = [accounts[0], accounts[1]];
    var nodeAddress = accounts[2];
    var amountsToStake = [1, 0.2];

    //0: 1/2 of Stake 
    //1: 1/4 of Stake
    var withdrawAmounts = [0.5, 0.05];

    var initialNodeBalance;
    var stakedAmount;
    var totalAmountStaked;

    it("should add a node", function() {
        return LinkPool.deployed().then(function (instance) {
            meta = instance;
        }).then(function () {
            return meta.addNode(nodeAddress);
        }).then(function () {
            return meta.getWallet.call(nodeAddress);
        }).then(function (address) {
            assert.equal(nodeAddress, address, "Address of node and the initial node address should be equal");
            return meta.getBalance.call(nodeAddress);
        }).then(function (ethBalance) {
            initialNodeBalance = web3.fromWei(ethBalance, 'ether');
            return meta.getAmountStaked.call(nodeAddress);
        }).then(function (amountStaked) {
            assert.equal(
                initialNodeBalance.toNumber(), 
                web3.fromWei(amountStaked.toNumber(), 'ether'), 
                "Amount staked on node should be equal to nodes starting balance"
            );
            return meta.getStakedAmount.call(nodeAddress, { from: nodeAddress });
        }).then(function (stakedAmount) {
            assert.equal(initialNodeBalance.toNumber(), web3.fromWei(stakedAmount.toNumber(), 'ether'), "Amount individualy staked should include the nodes initial balance");
        });
    });

    it("should allow staking", function() {
        return LinkPool.deployed().then(function(instance) {
            return meta.addStake(nodeAddress, { from: stakers[0], value: web3.toWei(amountsToStake[0], 'ether') });
        }).then(function () {
            return meta.getAmountStaked.call(nodeAddress);
        }).then(function (amountStaked) {
            assert.equal(
                initialNodeBalance.toNumber() + amountsToStake[0],
                web3.fromWei(amountStaked.toNumber(), 'ether'), 
                "Amount staked should have increased " + amountsToStake[0] + " ether"
            );
            return meta.getNumStakes.call(nodeAddress);
        }).then(function (numStakes) {
            assert.equal(numStakes.toNumber(), 2, "Number of stakes on the node should be 2")
        });
    });

    it("should be able to get a staker amount", function() {
        return LinkPool.deployed().then(function (instance) {
            return instance.getStakedAmount.call(nodeAddress);
        }).then(function (stake) {
            stakedAmount = stake;
            assert.equal(web3.fromWei(stake.toNumber(), 'ether'), amountsToStake[0], "Amount individually staked should be " + amountsToStake[0] + " ether");
        });
    });

    it("should be able to do a partial withdraw", function() {
        return LinkPool.deployed().then(function (instance) {
            return meta.withdrawStake(stakers[0], { from: nodeAddress, value: web3.toWei(withdrawAmounts[0], 'ether')});
        }).then(function (amount) {
            return meta.getAmountStaked.call(nodeAddress);
        }).then(function (amountStaked) {
            assert.equal(
                initialNodeBalance.toNumber() + (amountsToStake[0] - withdrawAmounts[0]), 
                web3.fromWei(amountStaked.toNumber(), 'ether'), 
                "Amount staked should be half of the amount staked"
            );
            return meta.getNumStakes.call(nodeAddress);
        }).then(function (numStakes) {
            assert.equal(numStakes.toNumber(), 2, "Number of stakes on the node should be 1 as it was a partial withdraw")
        });
    });

    it("should be able to do a full withdraw", function() {
        return LinkPool.deployed().then(function (instance) {
            return meta.withdrawStake(stakers[0], { from: nodeAddress, value: web3.toWei(withdrawAmounts[0], 'ether')});
        }).then(function (amount) {
            return meta.getAmountStaked.call(nodeAddress);
        }).then(function (amountStaked) {
            assert.equal(
                initialNodeBalance.toNumber(), 
                web3.fromWei(amountStaked.toNumber(), 'ether'), 
                "Amount staked should be same as initial node balance"
            );
            return meta.getNumStakes.call(nodeAddress);
        }).then(function (numStakes) {
            assert.equal(numStakes.toNumber(), 1, "Number of stakes on the node should be 1 as it was a full withdraw")
        });
    });

    it("should allow multiple stakes", function() {
        var totalAmountStaked = amountsToStake[0] + amountsToStake[1];
        return LinkPool.deployed().then(function(instance) {
            return meta.addStake(nodeAddress, { from: stakers[0], value: web3.toWei(amountsToStake[0], 'ether') });
        }).then(function () {
            return meta.addStake(nodeAddress, { from: stakers[1], value: web3.toWei(amountsToStake[1], 'ether') });
        }).then(function () {
            return meta.getAmountStaked.call(nodeAddress);
        }).then(function (amountStaked) {
            assert.equal(
                initialNodeBalance.toNumber() + totalAmountStaked,
                web3.fromWei(amountStaked.toNumber(), 'ether'), 
                "Amount staked should have increased by " + totalAmountStaked + " ether"
            );
            return meta.getNumStakes.call(nodeAddress);
        }).then(function (numStakes) {
            assert.equal(numStakes.toNumber(), 3, "Number of stakes on the node should be 3")
        });
    });

    it("should allow multiple stakes from the same staker", function() {
        totalAmountStaked = (amountsToStake[0] * 2) + amountsToStake[1];
        return LinkPool.deployed().then(function(instance) {
            return meta.addStake(nodeAddress, { from: stakers[0], value: web3.toWei(amountsToStake[0], 'ether') });
        }).then(function () {
            return meta.getAmountStaked.call(nodeAddress);
        }).then(function (amountStaked) {
            assert.equal(
                initialNodeBalance.toNumber() + totalAmountStaked,
                web3.fromWei(amountStaked.toNumber(), 'ether'), 
                "Amount staked should have increased by " + totalAmountStaked + " ether"
            );
            return meta.getNumStakes.call(nodeAddress);
        }).then(function (numStakes) {
            assert.equal(numStakes.toNumber(), 3, "Number of stakes on the node still should be 3")
        });
    });

    it("should allow to add interest earned back to stakers", function() {
        return LinkPool.deployed().then(function(instance) {
            return meta.addInterest({ from: nodeAddress });
        }).then(function () {
            return meta.getAmountStaked.call(nodeAddress);
        }).then(function (amountStaked) {
            assert.isAbove(web3.fromWei(amountStaked.toNumber(), 'ether'), totalAmountStaked, "Number staked in the node should be greater than total amount staked after adding interest")
        });
    });
});