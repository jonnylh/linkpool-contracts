App = {
  web3Provider: null,
  contracts: {},

  initWeb3: function() {
    // Is there is an injected web3 instance?
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    } else {
      // If no injected web3 instance is detected, fallback to the TestRPC
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
    }
    web3 = new Web3(App.web3Provider);

    return App.initContract();
  },

  initContract: function() {
    $.getJSON('LinkPool.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract
      var LinkPoolArtifact = data;
      App.contracts.LinkPool = TruffleContract(LinkPoolArtifact);
    
      // Set the provider for our contract
      App.contracts.LinkPool.setProvider(App.web3Provider);
    
      // Use our contract to create the first node
      return App.contracts.LinkPool.deployed().then(function(instance) {
        return instance.addNode.call().then(function (node) {
          var nodesRow = $('#nodesRow');
          var nodeTemplate = $('#nodeTemplate');
  
          nodeTemplate.find('.panel-title').text(node.wallet);
          nodeTemplate.find('.amount-staked').text(node.amountStaked);
          nodeTemplate.find('.num-addresses').text(node.addresses.length);
          nodeTemplate.find('.percentage-hold').text(node.amounts.length);
    
          nodesRow.append(nodeTemplate.html());
        })
      }).catch(function (e) {
        alert("Add node failed: " + e);
      });
    });
    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-adopt', App.handleAdopt);
  },
};

$(function() {
  $(window).load(function() {
    App.initWeb3();
  });
});
