pragma solidity ^0.4.11;

import "zeppelin-solidity/contracts/ownership/Ownable.sol";
import "zeppelin-solidity/contracts/math/SafeMath.sol"; 

contract LinkPool is Ownable {

    struct Node {
        address wallet;
        uint256 amountStaked;
        uint256 totalPaidOut;
        uint totalStakes;
        uint currentStakes;
        mapping (uint => address) stakeIndex;
        mapping (address => uint256) stakes;
    }

    mapping (address => Node) nodes;
    address[] nodeIndex;

    // Create a new node on the LinkPool
    function addNode(address _address) onlyOwner() public payable returns (address node) {
        nodes[_address] = Node(_address, _address.balance, 0, 0, 0);
        nodeIndex.push(_address);
        Node storage n = nodes[_address];
        n.stakes[_address] = _address.balance;
        n.stakeIndex[n.totalStakes] = _address;
        n.totalStakes++;
        n.currentStakes++;
        node = nodes[_address].wallet;
    }

    // Add a stake onto a node
    function addStake(address _address) public payable {
        require(msg.value > 0);
        uint256 _amount = msg.value;
        Node storage n = nodes[_address];
        n.wallet.transfer(_amount);
        if (n.stakes[msg.sender] == 0) {
            n.stakeIndex[n.totalStakes] = _address;
            n.totalStakes++;
            n.currentStakes++;
        }
        n.stakes[msg.sender] = SafeMath.add(n.stakes[msg.sender], _amount);
        n.amountStaked = SafeMath.add(n.amountStaked, _amount);
    }

    // Withdraw a stake from a node
    function withdrawStake(address staker) public payable {
        Node storage n = nodes[msg.sender];
        uint256 _amount = msg.value;
        uint256 stake = n.stakes[staker];
        require(stake >= _amount);
        staker.transfer(_amount);
        n.stakes[staker] = SafeMath.sub(stake, _amount);
        if (n.stakes[staker] == 0) {
            n.currentStakes--;
        }
        n.amountStaked = SafeMath.sub(n.amountStaked, _amount);
    }

    // Return the interest earned on the node back to the stakers
    function addInterest() public payable {
        require(n.wallet.balance > n.amountStaked);
        Node storage n = nodes[msg.sender];
        uint256 operatingIncome = SafeMath.sub(n.wallet.balance, n.amountStaked);
        for (uint i = 0; i < n.totalStakes; i++) {
            address staker = n.stakeIndex[i];
            uint256 stake = n.stakes[staker];
            if (stake > 0) {
                uint256 percentageStaked = SafeMath.div(stake, n.amountStaked);
                uint256 tokensEarned = SafeMath.mul(percentageStaked, operatingIncome);
                n.stakes[staker] = SafeMath.add(n.stakes[staker], tokensEarned);   
            }
        }
        n.amountStaked = SafeMath.add(n.amountStaked, operatingIncome);
        n.totalPaidOut = SafeMath.add(n.totalPaidOut, operatingIncome);
    }

    // Get the total number of nodes
    function getNodeLength() public constant returns (uint nodeLength) {
        nodeLength = nodeIndex.length;
    }

    // Get a node address by index
    function getNodeAddressByIndex(uint i) public constant returns (address nodeAddress) {
        require(i < nodeIndex.length);
        nodeAddress = nodeIndex[i];
    } 

    // Get a given Node from an address
    function getAmountStaked(address _address) public constant returns (uint256 amountStaked) {
        return nodes[_address].amountStaked;
    }

    // Get an amount staked per staker
    function getStakedAmount(address _address) public constant returns (uint256 amount) {
        return nodes[_address].stakes[msg.sender];
    }

    // Get a nodes wallet
    function getWallet(address _address) public constant returns (address wallet) {
        return nodes[_address].wallet;
    }

    // Get a number of stakers
    function getNumStakes(address _address) public constant returns (uint totalStakes) {
        return nodes[_address].currentStakes;
    }

    function getBalance(address _address) public constant returns (uint256 balance) {
        balance = _address.balance;
    }

}