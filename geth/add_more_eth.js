// Sending some ether to ours good friends
[1, 2, 3].forEach(function (v) {
  eth.sendTransaction({ from: eth.accounts[0], to: eth.accounts[v], value: web3.toWei(2) });
});
